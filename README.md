# Structure

This project is a starting point for a Flutter application.

## Version 1.0.3 dev

## Table of contents

- [Dependencies](#dependencies)
- [Folders and files strucutre](#folders-and-files-strucutre)
- [App Configuration](#app-configuration)
  - [App Builder](#app-builder)
  - [App Defaults](#defaults)
  - [User Roles](#roles)
  - [Roles Middleware](#role-middleware)
- [Constants](#constants)
- [Models](#models)
- [Repositories](#repositories)
- [Services](#services)
  - [State Management](#state-management)
  - [API Service](#api-service)
    - [Get Started](#get-started)
    - [Perform a request](#perform-a-request)
  - [Graphql Service](#graphql-service)
    - [Config](#configuration)
    - [Usage](#usage)
  - [Pagination](#pagination)
- [Styleing the app](#style)
- [Utils](#utils)
- [Main Components](#main-app-components)
- [Navigation](#navigation)
  - [Routes](#routes)
  - [Pass data](#pass-data)
  - [Push pages](#push-pages)
- [Translation](#translation)
- [Features of the app](#features)
  - [Splash screen](#splash-screen)
  - [Create a page](#creating-a-page)
  - [Folders and files](#folders-and-files)
- [Structure Snippets](#structure-vscode-snippets)
- [TODO](#todo)

## Dependencies

This structure depends on:

- **State Management**: using GetX package for state management, navigation, localization, dependency injection, controllers and services. [GetX](https://pub.dev/packages/get)
- **HTTP requests**: using dio package for http requests. [Dio](https://pub.dev/packages/dio)
- **Gql requests**: using graphql_flutter package for performing requests. [graphql_flutter](https://pub.dev/packages/graphql_flutter), using graphql_codegen for generating dart code from schema and documents. [graphql_codegen](https://pub.dev/packages/graphql_codegen)
- **Assets**: using fluttergen as dev dependency for generating assets. [Fluttergen](https://pub.dev/packages/flutter_gen)
- **Theme**: using theme_tailor as dev dependency for generating ThemeExtension. [theme_tailor](https://pub.dev/packages/theme_tailor)
- **Storage Caching**: using [flutter_secure_storage](https://pub.dev/packages/flutter_secure_storage) for caching sensitive like token and [get_storage](https://pub.dev/packages/get_storage) for caching normal data like user data.
- **Design pattern**: using MVC as a design pattern.

## Folders and files strucutre

![folder_strucutre](doc_assets/folder_strucutre.JPG "folder_strucutre")

## App Configuration

### App Builder

appBuilder is a [GetxService] witch control user role, localization, and storing user data in the storage

it has two types of storing data:

- Secured Storage: used to store user token
  [link](https://pub.dev/packages/flutter_secure_storage)
- GetStorage: used to store user data if exists and other information like local and user role
  [link](https://pub.dev/packages/get_storage)

AppBuilder also has another responsibility to initialize any service needed even services that depends on user role.

for example: apiService will initialized any way, but firebase service can be initialized just for registered user.

### Defaults

It is a reference to initialize app with default values.

```[Dart]
abstract class Default {
  static const AppLocalization defaultLocale = AppLocalization.en;

  static const Role defaultRole = NewUser();

  static const String appTitle = '';

  static preferredOrientation() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }
}
```

you can add for it default theme if needed, or default location if you have maps at your project to open map on specific location if user don't give app location permissions

### Roles

It is a constants values written as classes to use inheritance and facilitates using of role.

**Warning:** Role class must not be used as user role it is created just to be inherited and to make transformation fromString and toString.
**Warning:** Don't use these roles to check user type that defined from back-end, you must add user types as enum in models section, then define those user types as roles by creating roles inherited from User role.

We have four main roles:

- **UnregisteredUser**.

- **NewUser**:
  Witch extends from UnregisteredUser.
  It is refer to user that open app for the first time.
  you can use it if you have onboarding screens and when the user complete screens you must to change it is role to UnregisteredUser. `appBuilder.setRole(UnregisteredUser());`

- **User**:
  Refer to registered user, if you have types of user registered in your system you have to add any type as Role inhireted from User.
  for example:

```[Dart]
class Client extends User {
  const Client();
}

class Shop extends User {
  const Shop();
}
```

**Warning:** Don't forget to add new roles to fromString and toString functions at Role class.

```[Dart]
static Role fromString(String s) {
    switch (s) {
      //////
      case "client":
        return const Client();
      //////
    }
  }

  @override
  String toString() {
    switch (runtimeType) {
      ///////
      case Client:
        return "client";
      //////
    }
  }
```

- **Guest**:
  extends from role, but if guest actually registered in database and has token then Guest have to inherited from User.

Roles has two functions you can override if needed:
- landing: return the suitable page that must opened for this role after login or open the app. you must use it every where when user change his role, ex: (unregistered -> user, user -> unregistered).
- initialize: function that initialize some services depends on role.

### Role middleware

Used for check role of user or to do some actions if user has a specific role.
For example if you want to check if current user is a guest user:

```[Dart]
  static bool get checkIfGuest {
    AppBuilder appBuilder = Get.find();
    if (appBuilder.role is Guest) {
      return true;
    }
    return false;
  }
```

And you can use it for doing some action like open dialog every time guest trying to open some screens.

## Constants

This section contains contants values needed in the app.
It may contain:

- Break points: that help with responsivity.
- Controllers tags: that help with dependency injection for controllers.
- Localization: define supported locales in the app.
- Storages names: contain every storage name for GetStorage.
- Secrets: contain api keys like google map key, agora key, pusher key and any key needed for specificservice.

You can add any constants you need to this section, **but** it is better not to add constants related to the system flow.

## Models

Here you can add public and main models in your app, like user model.

## Repositories

You can create a repo for every feature in your app like `AuthRepo` witch contains static functions and every function perform a request (Rest api, graphql)
**Warning:** you can skip this layer when using APIService in this structure.

## Services

Here is a section to implement sevices that developer can create it, or to using a third party like http request, agora, ...etc.

### State management

Created to facilate the process of state managemet ecpecially when you need future data.

You can create a variable that refresh the UI by changing variable status.

```
enum VariableStatus {
  Loading,
  HasData,
  Error,
}
```

and you can check the status by:

```
if (data.loading){
  //
} else if (data.hasError){
  //
} else if (data.hasData){
  //
}
```

Taking null as initial value meaning the status will be loading.

```
ObsVar<Type> data = ObsVar(null);
ObsList<Type> list = ObsList(null);
```

you can force refreshing the UI by:

```
ObsVar<Type> data = ObsVar(null);
data.refresh();
```

you can reset data (data to null, status to loading) by:

```
ObsVar<Type> data = ObsVar(null);
data.reset;
```

Status changed in this cases:

```
ObsVar<String> data = ObsVar(null);

data.value = "coming data"; // status will be HasData
data.error = "No Internet"; //status will be hasError
data.reset; //status will be loading
```

**Warning:** UI refreshed by changing of status so if status not changed UI will not refreshed for example:

```
ObsVar<String> data = ObsVar(null);

data.value = "coming data"; // status will be HasData and refresh ui

data.value = "another data"; //status still HasData and ui will not refresh

// you need this to refresh ui
data.refresh();
```

- **ObsVar**:
  Used to handle a variable of any type.
- **ObsList**:
  Used to handle list of data and refreshing UI depending on list length, ObsList has a lot of built-in functions that work as list.

```
ObsList<String> list = ObsList(null);
// refreshing depends on status and list length
if (list.loading){

}else if (list.hasError){

}else if (list.hasData){
  // here must refresh on length
  list.valueLength;
}

// access elements
list[0];
list[i];

```

### API Service

A service used for request REST API depends on [dio](https://pub.dev/packages/dio).
The main feature that the service handle the request as object not just as Future function, so you can control the request by an instance.

#### Get started

First you have to initialize the Service, using this structure you must initialize it in [AppBuilder](#app-builder).

```[Dart]
Get.put(APIService(token: token));
```
The token variable is nullable.

you can also pass more parameters
```[Dart]
Get.put(APIService(
  token: token,
  language: locale.value,
  headers: {"Accept": "application/json"},
  withLog: true,
));
```

#### Perform a request

You have to create an instance of Request.

```[Dart]
Request request = Request(endPoint: EndPoints.something);
request.perform();
```

```
Request request = Request(
      endPoint: '', //required
      method: RequestMethod.Get, //api method 
      priority: RequestPriority.mid,
      type: RequestType.normal,
      header: {},
      copyHeader: {},
      params: {},
      body: {},
      cancelToken: CancelToken(),
      fromJson: (json) => json,
      onReceiveProgress: (count,total) => log("progress: ${count/total}"),
      onSendProgress: (count,total) => log("progress: ${count/total}"),
    );
```
- **endPoint**: Api end point.
- **method**: Api method, default is GET.
- **priority**: Indicate the priority of the request in the priority queue, default is [RequestPriority.mid].
- **type**: Type of request that indicate's if request can be executed multiple time [RequestMethod.normal] which it is the default, [RequestMethod.latest] will cancel any previous api with same end point in the queue, [RequestMethod.first] will cancel any coming api if there is an api with the same end point in the queue.
- **header**: Used for replace the default header defined in the service and use another header.
- **copyHeader**: Used to add some parameters for the defualt header to customize it for specific request.
- **params**: Parameters of the api.
- **body**: Body of the api.
- **fromJson**: Optional Parser for the response data.
- **onReceiveProgress**: Listener helps to display receiving data progress.
- **onSendingProgress**: Listener helps to display sending data progress, Don't use it with Get method.


### Graphql Service

A service that has client to handle gql queries and mutations.

#### Configuration

At folder config you must put your gql schema and documents (queries & mutations) then using [graphql_codegen](https://pub.dev/packages/graphql_codegen) whitch can generate queries and mutation as dart code instead writing them as Strings.
Graphql client implemented from [graphql_flutter](https://pub.dev/packages/graphql_flutter).

#### Usage

Initializing service support passing token and locale, so it will be declared at the header.
`GqlService.init(locale: "en", token: null);`

Performing requests implemeted at Repo layer.

### Pagination

## Style

This section handle every thing related to styling of the app, like:

- **Assets**: witch must be generated by fluttergen package, but if there any problem with it you can add assets paths in assets file.
- **Themes**: define supported themes.
- **App Style**: define colors, shadows, fonts, and cutomize themeData espscially text styles, input decoration theme and what your app using components that depends on theme.

## Utils

Or you can name it helper.
This section contains helper functions, extensions, and validation functions.

## Main App Components

This section Witch located in core/widgets contain app main widget, but not contain component related to app design, like app bar, bottom nav bar and product card.
It conatain component like loading widget, error widget and AppImage -widget to render any type of images-.

## Navigation

Handling Navigation in app depends on GetX package to route management.

### Routes

Navigation in this structure depends on named route, so you must to define all routes in your app.
Pages defined as enum, for example

```[Dart]
enum Pages {
  home;

  String get value => '/$name';
}
```

Page name should be written in snake_case. filtered products page => filtered_products
Value function is necessary because routes must strat with "/".

then you have to define GetPage.

```[Dart]
  static List<GetPage<dynamic>> routes() => [
    GetPage(name: Pages.home.value, page: () => HomePage()),
  ];
```

### Pass data
If page needs some parameters you have to use arguments to pass it.

- If the page need one parameter you can do that

```[Dart]
  static List<GetPage<dynamic>> routes() => [
    GetPage(name: Pages.product_details.value, page: () => ProductDetailsPage(id: Get.arguments)),
  ];
```
- If the page need a lot of parameters you have to create a model for it.

```[Dart]
  static List<GetPage<dynamic>> routes() => [
    //nav it is an object contain parameters needed in the page
    GetPage(name: Pages.filtered_products.value, page: () => FilteredProductsPage(nav: Get.arguments)),
  ];
```

### Push pages

using this functions which facilitates the syntax.

`Nav.to(Pages.product_details, arguments: selectedProduct.id);`

And contain three functions [to, replacement, offAll], at other cases use GetX Navigator.


## Translation

## Features

### Splash screen

There is an empty splash screen you can do anything with it (add an image, display a video), and splashController call init function of appBuilder which get all needed data then navigation to the right page.

### Creating a page

Since the structure depends on MVC design pattern, then the page structure also will be MVC.

![page_strucutre](doc_assets/page_structure.PNG "page_strucutre")

The page is a folder that contain:
- index.dart file: this is where you build ui.
- controller.dart file: this is where you build GetX controller and logic.
- models folder: contains necessary models for this page like navigation model, logical models that inherited from main models exists in core and logical models that isn't exist in core.
- widgets folder: contains page components. It shouldn't used outside this page.

### Folders and files

## Structure VsCode Snippets

## TODO

- [O] easy localization for localization.
- [O] add [flutter_staggered_grid_view](https://pub.dev/packages/flutter_staggered_grid_view) for the structure and for pagination.
- [O] create layers for api service (APIService , Client, Modelling and error handling).
- [O] documentation for api service.
- [O] documentation for pagination.
- [O] documentation for translation.
- [O] documentation for folders & files.
- [O] documentation for snippets.
