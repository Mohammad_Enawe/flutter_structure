import 'package:animated_theme_switcher/animated_theme_switcher.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'core/config/app_builder.dart';
import 'core/config/defaults.dart';
import 'core/localization/localization.dart';
import 'core/routes.dart';
import 'core/localization/translations.dart';
import 'features/splash_screen/index.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    Default.preferredOrientation();
    final AppBuilder appBuilder = Get.put(AppBuilder());
    return ThemeProvider(
      initTheme: Default.defaultTheme.theme,
      child: Obx(
        () => GetMaterialApp(
          debugShowCheckedModeBanner: false,
          title: Default.appTitle,
          theme: appBuilder.theme.theme,
          themeMode: ThemeMode.light,
          localizationsDelegates: const [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            DefaultCupertinoLocalizations.delegate,
          ],
          translations: Messages(),
          locale: appBuilder.locale.locale,
          fallbackLocale: AppLocalization.en.locale,
          supportedLocales:
              AppLocalization.values.map((e) => e.locale).toList(),
          initialRoute: '/',
          getPages: AppRouting.routes(),
          home: const SplashScreen(),
        ),
      ),
    );
  }
}
