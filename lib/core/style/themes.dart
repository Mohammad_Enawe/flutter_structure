import 'package:flutter/material.dart';
import 'package:structure/core/style/style.dart';

import '../config/defaults.dart';

enum AppTheme {
  light,
  dark,
  ;

  bool get isLight => this == light;
  bool get isDark => this == dark;

  static AppTheme fromString(String s) {
    AppTheme theme = Default.defaultTheme;
    for (var element in AppTheme.values) {
      if (element.name == s) {
        theme = element;
      }
    }
    return theme;
  }

  ThemeData get theme {
    switch (this) {
      case light:
        return AppStyle.lightTheme;
      case dark:
        return AppStyle.darkTheme;
    }
  }
}
