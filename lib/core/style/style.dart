import 'package:flutter/material.dart';
import 'package:theme_tailor_annotation/theme_tailor_annotation.dart';

part 'style.tailor.dart';

///This class must have the Themes only
abstract class AppStyle {
  static ThemeData lightTheme = ThemeData.light(
      // extensions: {const MyTheme(background: Colors.black)},
      // primaryColor: Colors.blue,
      // scaffoldBackgroundColor: Colors.grey[50],
      // textTheme: const TextTheme(),
      );

  static ThemeData darkTheme = ThemeData.dark(
      // extensions: {const MyTheme(background: Colors.black)},
      // primaryColor: Colors.blueGrey,
      // scaffoldBackgroundColor: Colors.blueGrey[900],
      // textTheme: const TextTheme(),
      );
}

// TODO - example of using tailor lib you can delete it
@TailorMixin()
class MyTheme extends ThemeExtension<MyTheme> with _$MyThemeTailorMixin {
  /// You can use required / named / optional parameters in the constructor
  // const MyTheme(this.background);
  // const MyTheme([this.background = Colors.blue])
  const MyTheme({required this.background});
  final Color background;
}
