import 'package:get/get.dart';

import '../../config/app_builder.dart';

class ThemeUtils {
  static T? value<T>({T? light, T? dark}) =>
      Get.find<AppBuilder>().theme.isLight ? light : dark;
}
