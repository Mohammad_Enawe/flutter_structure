// import 'dart:convert';
// import 'dart:math';
// import 'dart:typed_data';
// import 'dart:ui' as ui;

// import 'package:flutter/foundation.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/rendering.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:get/get.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';

// class LocationUtils {
//   static Future<LatLng?> getMyLocation({bool openSettings = true}) async {
//     LocationPermission permission = await Geolocator.checkPermission();
//     if (permission == LocationPermission.denied) {
//       permission = await Geolocator.requestPermission();
//       return getMyLocation(openSettings: openSettings);
//     }

//     if (permission == LocationPermission.deniedForever) {
//       Geolocator.openAppSettings();
//       return null;
//     }
//     if (!await Geolocator.isLocationServiceEnabled()) {
//       if (openSettings) await Geolocator.openLocationSettings();
//       return null;
//     }
//     Position position = await Geolocator.getCurrentPosition();
//     return LatLng(position.latitude, position.longitude);
//   }

//   static locationListener(Function(LatLng currentLocation) onChange) async {
//     LatLng? location = await getMyLocation(openSettings: false);
//     if (location != null) {
//       onChange(location);
//       Geolocator.getPositionStream().listen((position) {
//         LatLng currentLocation = LatLng(position.latitude, position.longitude);
//         onChange(currentLocation);
//       });
//     }
//   }

//   static double calculateDistance(LatLng l1, LatLng l2) {
//     var p = 0.017453292519943295;
//     var c = cos;
//     var a = 0.5 -
//         c((l2.latitude - l1.latitude) * p) / 2 +
//         c(l1.latitude * p) *
//             c(l2.latitude * p) *
//             (1 - c((l2.longitude - l1.longitude) * p)) /
//             2;
//     return 12742 * asin(sqrt(a)); //KM
//   }

//   static double distanceDependOnZoom(double zoom,
//       {double normativeZoom = 14, double normativeDistance = 0.5}) {
//     if (zoom == normativeZoom) {
//       return normativeDistance;
//     } else if (zoom < normativeZoom) {
//       return normativeDistance * (normativeZoom - zoom) * 2;
//     } else {
//       return normativeDistance / (normativeZoom - zoom) * 2;
//     }
//   }

//   static Future<Uint8List?> widgetToBytesNotInTree(
//     Widget widget, {
//     Duration wait = const Duration(milliseconds: 150),
//     double pixelRatio = 3,
//   }) async {
//     final RenderRepaintBoundary repaintBoundary = RenderRepaintBoundary();
//     final PipelineOwner pipelineOwner = PipelineOwner();
//     final BuildOwner buildOwner = BuildOwner(focusManager: FocusManager());
//     widget = widget;
//     Size logicalSize = ui.window.physicalSize /
//         MediaQueryData.fromWindow(ui.window).devicePixelRatio;

//     try {
//       final RenderView renderView = RenderView(
//         window: ui.window,
//         child: RenderPositionedBox(
//             alignment: Alignment.center, child: repaintBoundary),
//         configuration: ViewConfiguration(
//           size: logicalSize,
//         ),
//       );
//       pipelineOwner.rootNode = renderView;
//       renderView.prepareInitialFrame();
//       final RenderObjectToWidgetElement<RenderBox> rootElement =
//           RenderObjectToWidgetAdapter<RenderBox>(
//         container: repaintBoundary,
//         child: Directionality(
//           textDirection: ui.TextDirection.ltr,
//           child: Material(
//             color: Colors.transparent,
//             elevation: 0,
//             shadowColor: Colors.transparent,
//             child: MediaQuery(data: MediaQuery.of(Get.context!), child: widget),
//           ),
//         ),
//       ).attachToRenderTree(buildOwner);
//       buildOwner.buildScope(rootElement);
//       await Future.delayed(wait);
//       buildOwner.buildScope(rootElement);
//       buildOwner.finalizeTree();
//       pipelineOwner.flushLayout();
//       pipelineOwner.flushCompositingBits();
//       pipelineOwner.flushPaint();
//       final ui.Image image =
//           await repaintBoundary.toImage(pixelRatio: pixelRatio);
//       final ByteData? byteData =
//           await image.toByteData(format: ui.ImageByteFormat.png);
//       return byteData!.buffer.asUint8List();
//     } catch (e) {
//       return null;
//     }
//   }
// }
