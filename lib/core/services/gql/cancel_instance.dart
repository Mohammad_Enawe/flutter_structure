class CancelInstance {
  bool isCanceled = false;

  cancel() {
    isCanceled = true;
  }
}
