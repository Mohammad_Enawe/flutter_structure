import 'dart:developer';

import 'package:graphql_flutter/graphql_flutter.dart';

import '../../localization/localization.dart';
import 'constants.dart';
import 'cancel_instance.dart';

class GQLService {
  static late GraphQLClient client;

  static Map<String, String> header = {
    // "x-hasura-admin-secret": GqlConstants.hasura_secret_key,
    "content-type": "application/json",
  };

  static init({
    required AppLocalization locale,
    String? token,
  }) {
    header['Accept-language'] = locale.value;
    if (token != null) {
      header["Authorization"] = "Bearer $token";
    }
    _initializeClient();
  }

  static _initializeClient() {
    client = GraphQLClient(
      link: HttpLink(
        GqlConstants.gqlEndpoint,
        defaultHeaders: header,
      ),
      cache: GraphQLCache(store: InMemoryStore()),
    );
  }

  static setToken(String? token) {
    if (token == null) {
      header.remove("Authorization");
    } else {
      header['Authorization'] = "Bearer $token";
    }
    _initializeClient();
  }

  static setLanguage(AppLocalization locale) {
    header['Accept-language'] = locale.value;
    _initializeClient();
  }

  static mutate<TParsed>(MutationOptions<TParsed> options,
      {CancelInstance? cancel}) async {
    String operationName = "Mutation";
    try {
      operationName = options.parserFn.runtimeType.toString().split("\$").last;
    } catch (_) {}
    log("######## Mutation $operationName ##########", name: "GQLSERVICE");
    log("$header", name: "Header");
    log("############################", name: "GQLSERVICE");
    log("${options.variables}", name: "Variables");
    log("############################", name: "GQLSERVICE");

    QueryResult result = await client.mutate(options);

    if (cancel != null && cancel.isCanceled) {
      log("######## Mutation $operationName ##########", name: "GQLSERVICE");
      log("Cancelled", name: "GQLSERVICE");
      log("############################", name: "GQLSERVICE");
      return null;
    }

    log("######## Mutation $operationName ##########", name: "GQLSERVICE");
    log("$result", name: "GQLSERVICE");
    log("############################", name: "GQLSERVICE");

    return result;
  }

  static query<TParsed>(QueryOptions<TParsed> options,
      {CancelInstance? cancel}) async {
    String operationName = "Query";
    try {
      operationName = options.parserFn.runtimeType.toString().split("\$").last;
    } catch (_) {}
    log("######## $operationName ##########", name: "GQLSERVICE");
    log("$header", name: "Header");
    log("############################", name: "GQLSERVICE");
    log("${options.variables}", name: "Variables");
    log("############################", name: "GQLSERVICE");

    QueryResult result = await client.query(options);

    if (cancel != null && cancel.isCanceled) {
      log("######## Mutation $operationName ##########", name: "GQLSERVICE");
      log("Cancelled", name: "GQLSERVICE");
      log("############################", name: "GQLSERVICE");
      return null;
    }

    log("######## $operationName ##########", name: "GQLSERVICE");
    log("$result", name: "GQLSERVICE");
    log("############################", name: "GQLSERVICE");

    return result;
  }
}

extension QueryResultExt on QueryResult {
  bool get success => !hasException;

  bool get isNoInternet {
    if (hasException) {
      return exception!.linkException != null;
    } else {
      return false;
    }
  }

  String? get errorMessage {
    if (hasException) {
      if (isNoInternet) {
        return "No Internet";
      }
      if (exception!.graphqlErrors.isNotEmpty) {
        return exception!.graphqlErrors.first.message;
      }
    }
    return null;
  }
}
