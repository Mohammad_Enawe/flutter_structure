// import 'dart:developer';

// import 'package:get/get.dart';
// import 'package:mapfiy/core/constants/controllers_tags.dart';
// import 'package:mapfiy/core/constants/enum.dart';
// import 'package:mapfiy/core/constants/role.dart';
// import 'package:mapfiy/features/social/social_details/index.dart';
// import 'package:share_plus/share_plus.dart';
// import 'package:uni_links/uni_links.dart';

// import '../routes.dart';

// linkListener() {
//   linkStream.listen(
//     (link) => DeepLinks.linkHandler(link, false),
//     onError: (e) {
//       log('$e');
//     },
//   );
// }

// class DeepLinks {
//   static const String deepLinkBasaeUrl = 'https://mapify.ixcoders.com/app/';

//   static shareProfileLink(int id, Role role, String name) async {
//     String link = deepLinkBasaeUrl + 'profile/${role.name}/$id';
//     String message =
//         'Press this link to enjoy with $name at Mapy World\n\n$link';
//     await Share.share(message);
//   }

//   static shareSocialLink(int id, SocialType type, String name) {
//     String link = deepLinkBasaeUrl + 'social/$id';
//     String message =
//         'Press this link to enjoy ${type.name} by $name at Mapy worlds\n\n$link';
//     Share.share(message);
//   }

//   static Future<bool> initialLink() async {
//     String? link = await getInitialLink();
//     if (link != null) {
//       log('open app by $link');
//       return linkHandler(link, true);
//     } else {
//       return false;
//     }
//   }

//   static bool linkHandler(String? link, bool background) {
//     if (link != null) {
//       link = link.replaceAll(deepLinkBasaeUrl, '');
//       List data = link.split('/');
//       try {
//         if (data[0] == 'profile') {
//           Role role = AppEnum.getRoleFromName(data[1]);
//           int id = int.parse(data[2]);
//           if (role != Role.client || role == Role.shop) {
//             return false;
//           }
//           if (background) {
//             Nav.offAll(
//               Pages.other_profile,
//               arguments: {'id': id, 'is_shop': role == Role.shop},
//             );
//           } else {
//             Nav.to(
//               Pages.other_profile,
//               arguments: {'id': id, 'is_shop': role == Role.shop},
//             );
//           }
//         } else if (data[0] == 'social') {
//           int id = int.parse(data[1]);
//           if (background) {
//             Get.offAll(() =>
//                 SocialDetailsPage(id: id, tag: ControllersTags.socialTag(id)));
//           } else {
//             Get.to(() =>
//                 SocialDetailsPage(id: id, tag: ControllersTags.socialTag(id)));
//           }
//         } else {
//           return false;
//         }
//       } catch (_) {
//         return false;
//       }
//       return true;
//     } else {
//       return false;
//     }
//   }
// }
