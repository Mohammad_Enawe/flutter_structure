// import 'dart:convert';
// import 'dart:developer';

// import 'package:firebase_core/firebase_core.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
// import 'package:flutter_local_notifications/flutter_local_notifications.dart';
// import 'package:get/get.dart';
// import 'package:mapfiy/core/constants/controllers_tags.dart';
// import 'package:mapfiy/core/constants/enum.dart';
// import 'package:mapfiy/core/models/app/notification.dart';
// import 'package:mapfiy/core/routes.dart';
// import 'package:mapfiy/core/utils/firebase_options.dart';
// import 'package:mapfiy/features/chat/models/message.dart' as messageModel;
// import 'package:mapfiy/features/chat/pages/chats/controller.dart';
// import 'package:mapfiy/features/chat/pages/room/controller.dart';
// import 'package:mapfiy/features/social/social_details/index.dart';

// Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
//   FirebaseApp app = await Firebase.initializeApp(
//     // name: 'mapfiy',
//     options: DefaultFirebaseOptions.currentPlatform,
//   );
//   log('Initialized default app $app');

//   log("Handling a background message: ${message.messageId}");
//   NotificationController notificationController =
//       Get.put(NotificationController());
//   notificationController.getNotification(message, isBackGround: true);
// }

// class NotificationController extends GetxController {
//   late FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

//   late FirebaseMessaging firebaseMessaging;

//   Future<String?> getToken() async {
//     String? token;
//     try {
//       token = await firebaseMessaging.getToken(vapidKey: "BGpdLRs......");
//     } catch (e) {
//       log(e.toString());
//     }
//     log('FCM_TOKEN: $token');
//     return token;
//   }

//   void fireBase() async {
//     FirebaseMessaging.onMessage.listen(getNotification);
//     FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);
//     firebaseMessaging.setForegroundNotificationPresentationOptions(
//         alert: true, badge: true, sound: true);
//   }

//   getNotification(RemoteMessage message, {bool isBackGround = false}) async {
//     log('Message');
//     log('${message.toMap()}');
//     // var data = message.data;
//     // showMessage(
//     //   id: int.parse(data['id']),
//     //   name: data['name'],
//     //   message: data['message'],
//     //   date: DateTime.now(),
//     //   base64: data['base64'],
//     //   payload: data.toString(),
//     // );
//     // return;
//     NotificationModel notification = NotificationModel.fromJson(message.data);
//     if (isBackGround) {
//       await init(isBackGroundInit: true);
//     }
//     switch (notification.type) {
//       case NotificationType.chat:
//         try {
//           RoomPageController roomController = Get.find<RoomPageController>();
//           if (roomController.room.id ==
//               (notification.data as messageModel.Message).roomId) {
//             roomController.getMessage(
//                 (notification.data as messageModel.Message)..isMine = false);
//           } else {
//             throw '';
//           }
//         } catch (_) {
//           try {
//             MyChatsPageController chatsController = Get.find();
//             chatsController
//                 .getMessage((notification.data as messageModel.Message));
//           } catch (_) {
//             showNotification(
//               id: notification.id,
//               title: notification.title,
//               payload: jsonEncode(message.data),
//               subTitle: notification.body,
//             );
//           }
//         }
//         break;
//       default:
//         showNotification(
//           id: notification.id,
//           title: notification.title,
//           payload: 'notification',
//           subTitle: notification.body,
//         );
//     }
//   }

//   bool notificationClick(NotificationResponse details) {
//     if (details.payload != null) {
//       log('###########################');
//       log(details.payload!);
//       log('##############################');
//       Map<String, dynamic>? data;
//       try {
//         data = jsonDecode(details.payload!);
//       } catch (_) {}
//       if (data != null) {
//         NotificationModel notification = NotificationModel.fromJson(data);
//         Map navMap = {
//           NotificationType.chat: Nav.to(Pages.chat),
//           NotificationType.follow: Nav.to(Pages.other_profile, arguments: {
//             'is_shop': notification.gender == GenderType.Shop,
//             'id': notification.data
//           }),
//           NotificationType.social: Get.to(() => SocialDetailsPage(
//                 id: notification.data,
//                 tag: ControllersTags.socialTag(notification.data),
//               )),
//           NotificationType.tag: Get.to(() => SocialDetailsPage(
//                 id: notification.data,
//                 tag: ControllersTags.socialTag(notification.data),
//               )),
//         };
//         if (notification.type == NotificationType.chat) {
//           Nav.to(Pages.chat);
//         }
//       }
//     }
//     return false;
//   }

//   showNotification({
//     required int id,
//     required String title,
//     required String payload,
//     required String subTitle,
//   }) async {
//     var android = AndroidNotificationDetails(
//       'mapfiy',
//       'mapfiy_channel',
//       importance: Importance.max,
//       priority: Priority.max,
//       groupKey: 'my group',
//       setAsGroupSummary: true,
//       enableVibration: true,
//       groupAlertBehavior: GroupAlertBehavior.all,
//       styleInformation: InboxStyleInformation(
//         [subTitle],
//         contentTitle: title,
//         summaryText: 'mapfiy',
//       ),
//     );
//     var ios = DarwinNotificationDetails();
//     var platform = NotificationDetails(android: android, iOS: ios);
//     await flutterLocalNotificationsPlugin.show(id, title, subTitle, platform,
//         payload: payload);
//   }

//   showMessage({
//     required int id,
//     required String name,
//     required String message,
//     required DateTime date,
//     required String base64,
//     required String payload,
//   }) async {
//     var android = AndroidNotificationDetails(
//       'mapfiy',
//       'mapfiy_channel',
//       importance: Importance.max,
//       priority: Priority.max,
//       groupKey: 'my group',
//       setAsGroupSummary: true,
//       enableVibration: true,
//       groupAlertBehavior: GroupAlertBehavior.all,
//       styleInformation: MessagingStyleInformation(
//         Person(name: name, icon: ByteArrayAndroidIcon.fromBase64String(base64)),
//         messages: [
//           Message(
//             message,
//             date,
//             Person(
//               name: name,
//               icon: ByteArrayAndroidIcon.fromBase64String(base64),
//             ),
//           ),
//           Message(
//             message,
//             date,
//             Person(
//               name: name,
//               icon: ByteArrayAndroidIcon.fromBase64String(base64),
//             ),
//           ),
//           Message(
//             message,
//             date,
//             Person(
//               name: name,
//               icon: ByteArrayAndroidIcon.fromBase64String(base64),
//             ),
//           ),
//         ],
//       ),
//     );
//     var ios = DarwinNotificationDetails();
//     var platform = NotificationDetails(android: android, iOS: ios);
//     await flutterLocalNotificationsPlugin.show(id, name, message, platform,
//         payload: payload);
//   }

//   bool openThroughNotification() {
//     if (_navigation != null) {
//       return _navigation!.call();
//     }
//     return false;
//   }

//   bool Function()? _navigation;
//   init({bool isBackGroundInit = false}) async {
//     firebaseMessaging = FirebaseMessaging.instance;
//     RemoteMessage? message = await firebaseMessaging.getInitialMessage();
//     if (message != null) {
//       //TODO:
//       _navigation = () => false;
//     }
//     flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
//     NotificationAppLaunchDetails? notification =
//         await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();
//     if (notification != null &&
//         notification.didNotificationLaunchApp &&
//         notification.notificationResponse != null) {
//       _navigation = () => notificationClick(notification.notificationResponse!);
//     }

//     //andoid 13
//     if (GetPlatform.isAndroid)
//       flutterLocalNotificationsPlugin
//           .resolvePlatformSpecificImplementation<
//               AndroidFlutterLocalNotificationsPlugin>()!
//           .requestPermission();

//     var android = AndroidInitializationSettings('@mipmap/ic_launcher');
//     var ios = DarwinInitializationSettings();
//     var initSettings = InitializationSettings(android: android, iOS: ios);
//     flutterLocalNotificationsPlugin.initialize(
//       initSettings,
//       onDidReceiveNotificationResponse: (details) => notificationClick(details),
//     );
//     if (!isBackGroundInit) {
//       NotificationSettings settings = await firebaseMessaging.requestPermission(
//         alert: true,
//         announcement: false,
//         badge: true,
//         carPlay: false,
//         criticalAlert: false,
//         provisional: false,
//         sound: true,
//       );

//       log('User granted permission: ${settings.authorizationStatus}');
//       fireBase();
//     }
//   }

//   @override
//   void onInit() {
//     super.onInit();
//   }
// }
