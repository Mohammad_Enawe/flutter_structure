// import 'package:pusher_client/pusher_client.dart';

// class Pusher {
//   static const String appKey = '66095962109e77f123bf';
//   static const String host = 'https://ex-cu.com';
//   // static const int wsPort = 6001;
//   static const String hostAuth = 'https://ex-cu.com/api/broadcasting/auth';
//   static const String cluster = 'us2';

//   late PusherOptions options;
//   late PusherClient pusher;
//   late Channel channel;
//   late String channelName;

//   //===== VARIABLES =======
//   final String eventName;
//   final Function(PusherEvent) onGetData;

//   getEvent(PusherEvent? event) {
//     print(event);
//     if (event != null) {
//       print('\n########## DATA ############');
//       print(event.data);
//       print('######################\n');
//       onGetData(event);
//     }
//   }

//   onConnectionStatechange(state) async {
//     print('\n########## STATE ############');
//     print(
//         "previousState: ${state!.previousState}, currentState: ${state.currentState}");
//     print('######################\n');
//     String? socketId = pusher.getSocketId();
//     print("socket id: $socketId");
//     print("channel name: $channelName");
//   }

//   onError(error) {
//     print('\n########## ERROR ############');
//     print("error: ${error!.message}");
//     print("code: ${error.code}");
//     print("exception: ${error.exception}");
//     print('######################\n');
//   }

//   Pusher(
//       {required String token,
//       required int id,
//       required this.eventName,
//       required this.onGetData}) {
//     options = PusherOptions(
//       host: host,
//       // wsPort: wsPort,
//       encrypted: true,
//       cluster: cluster,
//       auth: PusherAuth(
//         hostAuth,
//         headers: {
//           'Authorization': 'Bearer $token',
//           'Content-Type': 'application/json',
//           'Accept': 'application/json',
//         },
//       ),
//     );

//     pusher = PusherClient(
//       appKey,
//       options,
//       autoConnect: false,
//       enableLogging: true,
//     );

//     channelName = 'private-user.$id';

//     init();
//   }

//   init() async {
//     await pusher.connect();
//     pusher.onConnectionStateChange(onConnectionStatechange);
//     pusher.onConnectionError(onError);
//     try {
//       await pusher.unsubscribe(channelName);
//     } catch (e) {}
//     channel = pusher.subscribe(channelName);
//     channel.bind(eventName, getEvent);
//   }

//   disconnect() async {
//     try {
//       await channel.unbind(eventName);
//     } catch (_) {}
//     try {
//       await pusher.unsubscribe(channelName);
//     } catch (_) {}
//     try {
//       await pusher.disconnect();
//     } catch (_) {}
//   }
// }
