import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../models/request.dart';
import '../models/response_model.dart';

// ignore: must_be_immutable
class RequestRefresher extends StatelessWidget {
  final Request request;
  final Widget? Function(BuildContext context, Object data) builder;
  final Function(Object data)? onDataFitched;
  final Widget Function(BuildContext context)? loader;
  final Widget Function(BuildContext context, ResponseModel response)?
      errorBuilder;
  final bool withRefresh;
  final Future<void> Function()? onRefresh;

  RequestRefresher({
    super.key,
    required this.request,
    required this.builder,
    this.onDataFitched,
    this.loader,
    this.errorBuilder,
    this.withRefresh = false,
    this.onRefresh,
  }) : assert(request.fromJson != null);

  bool _dataFitched = false;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        if (request.isLoading || request.isPending) {
          return loader == null ? const _LoadingWidget() : loader!(context);
        } else {
          if (request.response!.success) {
            var parsedData = request.response!.data;
            if (onDataFitched != null && !_dataFitched) {
              _dataFitched = true;
              onDataFitched!(parsedData);
            }

            if (withRefresh) {
              return RefreshIndicator(
                onRefresh:
                    onRefresh != null ? onRefresh! : () => request.refresh(),
                child: builder(context, parsedData) ?? const SizedBox(),
              );
            }
            return builder(context, parsedData) ?? const SizedBox();
          } else {
            return errorBuilder?.call(context, request.response!) ??
                (withRefresh
                    ? RefreshIndicator(
                        onRefresh: onRefresh != null
                            ? onRefresh!
                            : () => request.refresh(),
                        child: _ErrorWidget(error: request.response!.message))
                    : _ErrorWidget(error: request.response!.message));
          }
        }
      },
    );
  }
}

class _ErrorWidget extends StatelessWidget {
  final String error;
  const _ErrorWidget({required this.error});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12.0),
                child: Text(
                  error,
                  maxLines: 3,
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),
        ScrollConfiguration(
          behavior: const MaterialScrollBehavior().copyWith(overscroll: false),
          child: SingleChildScrollView(
            child: SizedBox(height: Get.height + 1, width: Get.width),
          ),
        )
      ],
    );
  }
}

class _LoadingWidget extends StatelessWidget {
  const _LoadingWidget();

  @override
  Widget build(BuildContext context) {
    return const Center(child: CircularProgressIndicator());
  }
}
