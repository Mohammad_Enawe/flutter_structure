import 'dart:async';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:get/get.dart' hide Response, FormData, MultipartFile;

import 'constants/end_points.dart';
import 'constants/messages.dart';
import 'constants/api_error.dart';
import 'handlers/error_handler.dart';
import 'handlers/success_handler.dart';
import 'logger/logger.dart';
import 'models/exceptions.dart';
import 'models/request.dart';
import 'models/response_model.dart';
import 'utilitis/queue.dart';

class APIService extends GetxService {
  //=========== CONFIGURATION ===========
  late bool withLog;
  late Map<String, dynamic> _headers;
  late Dio _dio;

  APIService({
    this.withLog = true,
    Map<String, dynamic>? headers,
    String? token,
    String? language,
  }) {
    _headers = headers ?? {};
    if (token != null) {
      _headers["Authorization"] = "Bearer $token";
    }
    if (language != null) {
      _headers["Accept-Language"] = language;
    }
    _dio = Dio();
    _dio.options = BaseOptions(
      headers: _headers,
      responseType: ResponseType.json,
    );
    if (withLog) headerLogger(_headers);
  }

  setToken(String? token) {
    if (token == null) {
      _headers.remove("Authorization");
    } else {
      _headers["Authorization"] = "Bearer $token";
    }
    _dio.options = _dio.options.copyWith(headers: _headers);
    headerLogger(_headers);
  }

  setLanguage(String language) {
    _headers["Accept-Language"] = language;
    _dio.options = _dio.options.copyWith(headers: _headers);
    headerLogger(_headers);
  }

  // ===================== PRIORITY QUEUE ===================
  ///Queue that listed requested api's
  final PriorityQueue _queue = PriorityQueue();

  ///Get all pending requests in the queue
  List<Request> get pendingsApis =>
      _queue.queue.where((e) => e.isPending).toList();

  ///Get all requested and not ended api's in the queue
  List<Request> get currentApis =>
      _queue.queue.where((e) => e.isLoading).toList();

  ///Add [Request] to our [PriorityQueue]
  ///you can use the listener in this function that help you to listen on [Request] by its [RequestStatus]
  Future<ResponseModel> request({
    required Request request,
    bool forceRequest = false,
  }) async {
    requestingLogger(request);
    if (forceRequest) request.priority = RequestPriority.high;
    _queue.add(request);
    Completer<ResponseModel> completer = Completer<ResponseModel>();
    Worker worker = ever<RequestStatus>(request.status, (status) {
      if (status == RequestStatus.done) {
        _queueRefresh();
        completer.complete(request.response);
      }
      if (withLog) _queue.queueLog();
    });
    _queueRefresh(forceRequest: forceRequest);
    ResponseModel response = await completer.future;
    worker.dispose();
    return response;
  }

  _queueRefresh({bool forceRequest = false}) {
    _queue.refresh();
    if (currentApis.isEmpty && pendingsApis.isEmpty) {
      return;
    } else if (forceRequest || currentApis.isEmpty) {
      List<Request> requests = _queue.pop();
      for (var request in requests) {
        request.status.value = RequestStatus.loading;
        requestAPI(request);
      }
    }
  }

  requestAPI(Request request) async {
    switch (request.method) {
      case RequestMethod.Get:
        request.response = await getData(request);
        break;
      case RequestMethod.Post:
        request.response = await postData(request);
        break;
      case RequestMethod.Put:
        request.response = await putData(request);
        break;
      case RequestMethod.Delete:
        request.response = await deleteData(request);
        break;
    }
    request.status.value = RequestStatus.done;
    resultLogger(request);
  }

  //============== API Methods ================

  ///Get method
  Future<ResponseModel> getData(Request request) async {
    String fullEndPoint = request.isFullURL
        ? request.endPoint
        : EndPoints.baseUrl + request.endPoint;
    Response? response;
    ResponseModel responseModel;
    Map<String, dynamic> requestHeader;
    if (request.header != null) {
      requestHeader = Map<String, dynamic>.from(request.header!);
    } else {
      requestHeader = Map<String, dynamic>.from(_headers);
    }
    if (request.copyHeader != null) {
      for (var key in request.copyHeader!.keys) {
        requestHeader[key] = request.copyHeader![key];
      }
    }
    if (withLog) {
      log(requestHeader.toString(), name: "API SERVICE");
    }
    try {
      response = await _dio.get(
        fullEndPoint,
        options: Options(headers: requestHeader),
        queryParameters: request.params,
        cancelToken: request.cancelToken,
        onReceiveProgress: request.onReceiveProgress,
      );
      responseModel = responseModelling(response, fromJson: request.fromJson);
    } on ModellingException catch (error) {
      responseModel = ResponseModel(
          success: false, errorType: ModellingError(), message: error.message);
    } on DioException catch (error) {
      responseModel = mainErrorHandler(error);
    } catch (e) {
      responseModel =
          ResponseModel(success: false, data: e, message: 'some error')
            ..statusCode = 0;
    }
    return responseModel;
  }

  ///Post method
  Future<ResponseModel> postData(Request request) async {
    String fullEndPoint = request.isFullURL
        ? request.endPoint
        : EndPoints.baseUrl + request.endPoint;
    Response? response;
    ResponseModel responseModel;
    Map<String, dynamic> requestHeader;
    if (request.header != null) {
      requestHeader = Map<String, dynamic>.from(request.header!);
    } else {
      requestHeader = Map<String, dynamic>.from(_headers);
    }
    if (request.copyHeader != null) {
      for (var key in request.copyHeader!.keys) {
        requestHeader[key] = request.copyHeader![key];
      }
    }
    if (withLog) {
      log(requestHeader.toString(), name: "API SERVICE");
    }
    try {
      response = await _dio.post(
        fullEndPoint,
        options: Options(headers: requestHeader),
        data: request.body,
        queryParameters: request.params,
        cancelToken: request.cancelToken,
        onReceiveProgress: request.onReceiveProgress,
        onSendProgress: request.onSendProgress,
      );
      responseModel = responseModelling(response);
    } on ModellingException catch (error) {
      responseModel = ResponseModel(
          success: false, errorType: ModellingError(), message: error.message);
    } on DioException catch (error) {
      responseModel = mainErrorHandler(error);
    } catch (e) {
      responseModel =
          ResponseModel(success: false, data: e, message: 'some error')
            ..statusCode = 0;
    }
    return responseModel;
  }

  ///Put method
  Future<ResponseModel> putData(Request request) async {
    String fullEndPoint = request.isFullURL
        ? request.endPoint
        : EndPoints.baseUrl + request.endPoint;
    Response? response;
    ResponseModel responseModel;
    Map<String, dynamic> requestHeader;
    if (request.header != null) {
      requestHeader = Map<String, dynamic>.from(request.header!);
    } else {
      requestHeader = Map<String, dynamic>.from(_headers);
    }
    if (request.copyHeader != null) {
      for (var key in request.copyHeader!.keys) {
        requestHeader[key] = request.copyHeader![key];
      }
    }
    if (withLog) {
      log(requestHeader.toString(), name: "API SERVICE");
    }
    try {
      response = await _dio.put(
        fullEndPoint,
        options: Options(headers: requestHeader),
        data: request.body,
        queryParameters: request.params,
        cancelToken: request.cancelToken,
        onReceiveProgress: request.onReceiveProgress,
        onSendProgress: request.onSendProgress,
      );
      responseModel = responseModelling(response);
    } on ModellingException catch (error) {
      responseModel = ResponseModel(
          success: false, errorType: ModellingError(), message: error.message);
    } on DioException catch (error) {
      responseModel = mainErrorHandler(error);
    } catch (e) {
      responseModel =
          ResponseModel(success: false, data: e, message: 'some error')
            ..statusCode = 0;
    }
    return responseModel;
  }

  ///Delete method
  Future<ResponseModel> deleteData(Request request) async {
    String fullEndPoint = request.isFullURL
        ? request.endPoint
        : EndPoints.baseUrl + request.endPoint;
    Response? response;
    ResponseModel responseModel;
    Map<String, dynamic> requestHeader;
    if (request.header != null) {
      requestHeader = Map<String, dynamic>.from(request.header!);
    } else {
      requestHeader = Map<String, dynamic>.from(_headers);
    }
    if (request.copyHeader != null) {
      for (var key in request.copyHeader!.keys) {
        requestHeader[key] = request.copyHeader![key];
      }
    }
    if (withLog) {
      log(requestHeader.toString(), name: "API SERVICE");
    }
    try {
      response = await _dio.delete(
        fullEndPoint,
        options: Options(headers: requestHeader),
        data: request.body,
        queryParameters: request.params,
        cancelToken: request.cancelToken,
      );
      responseModel = responseModelling(response);
    } on ModellingException catch (error) {
      responseModel = ResponseModel(
          success: false, errorType: ModellingError(), message: error.message);
    } on DioException catch (error) {
      responseModel = mainErrorHandler(error);
    } catch (e) {
      responseModel =
          ResponseModel(success: false, data: e, message: 'some error')
            ..statusCode = 0;
    }
    return responseModel;
  }

  stop(Request request) {
    if (request.isPending) {
      request.response = ResponseModel(
        success: false,
        message: APIErrorMessages.canceled,
        errorType: CANCELED(),
      );
      _queue.queue.remove(request);
      request.status.value = RequestStatus.done;
    } else {
      request.cancelToken.cancel();
    }
  }
}
