// ignore_for_file: constant_identifier_names, prefer_typing_uninitialized_variables

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../api_service.dart';
import '../widgets/request_refresher.dart';
import 'response_model.dart';

enum RequestMethod { Get, Post, Put, Delete }

enum RequestStatus { pending, loading, done }

enum RequestPriority { low, mid, high }

enum RequestType { normal, latest, first }

class Request<T> {
  ///Type of request [RequestMethod]
  RequestMethod method;

  ///Api end point
  String endPoint;

  ///Is the end point has the base url or not
  bool isFullURL;

  ///Priority of the request
  RequestPriority priority;

  ///Type of request that indicate if request can be executed multiple time [RequestMethod.normal] which it is the default
  ///[RequestType.latest] will cancel any previous api with same end point in the queue
  ///[RequestType.first] will cancel any coming api if there is an api with the same end point in the queue
  RequestType type;

  ///Used for deactivate request
  late CancelToken cancelToken;

  ///Custom header
  Map<String, dynamic>? header;

  ///Copy header
  Map<String, dynamic>? copyHeader;

  ///Request params
  Map<String, dynamic>? params;

  ///Body of the Request
  var body;

  ///Response of this request
  ResponseModel? response;

  ///Listener for receiving data
  Function(int count, int total)? onReceiveProgress;

  ///Listener for sending data
  Function(int count, int total)? onSendProgress;

  ///Status of request in queue
  Rx<RequestStatus> status = RequestStatus.pending.obs;

  bool get isPending => status.value == RequestStatus.pending;
  bool get isLoading => status.value == RequestStatus.loading;
  bool get isDone => status.value == RequestStatus.done;

  ///Optional Modelling
  Function(Map<String, dynamic> json)? fromJson;

  Request({
    required this.endPoint,
    this.isFullURL = false,
    this.method = RequestMethod.Get,
    this.priority = RequestPriority.mid,
    this.type = RequestType.normal,
    this.header,
    this.copyHeader,
    this.params,
    this.body,
    CancelToken? cancelToken,
    this.fromJson,
    this.onReceiveProgress,
    this.onSendProgress,
  }) {
    if (header != null && copyHeader != null) {
      throw Exception(
          "Can't pass both header and copyHeader when creating Request instance");
    }
    if (method == RequestMethod.Get &&
        (body != null || onSendProgress != null)) {
      throw Exception(
          "Get request must not have body or onSendProgress parameters");
    }
    if (cancelToken == null) {
      this.cancelToken = CancelToken();
    } else {
      this.cancelToken = cancelToken;
    }
  }

  Future<ResponseModel> perform() async {
    APIService service = Get.find();
    ResponseModel response = await service.request(request: this);
    if (fromJson != null && response.data is List) {
      List<T> temp = [];
      for (var element in response.data) {
        temp.add(element as T);
      }
      response.data = temp;
    }
    return response;
  }

  stop() {
    Get.find<APIService>().stop(this);
  }

  Future refresh() async {
    if (status.value != RequestStatus.done) {
      stop();
    }
    status.value = RequestStatus.pending;
    response = null;
    perform();
  }

  reset() {
    status.value = RequestStatus.pending;
    response = null;
  }

  Widget listBuilder({
    required Widget? Function(BuildContext context, List<T> data) builder,
    Function(List<T> data)? onDataFitched,
    Widget Function(BuildContext context)? loader,
    Widget Function(BuildContext context, ResponseModel response)? errorBuilder,
    bool withRefresh = false,
    Future<void> Function()? onRefresh,
  }) =>
      RequestRefresher(
        request: this,
        builder: (context, data) => builder(context, data as List<T>),
        onDataFitched: onDataFitched != null
            ? (data) => onDataFitched(data as List<T>)
            : null,
        loader: loader,
        errorBuilder: errorBuilder,
        withRefresh: withRefresh,
        onRefresh: onRefresh,
      );

  Widget objectBuilder({
    required Widget? Function(BuildContext context, T data) builder,
    Function(T data)? onDataFitched,
    Widget Function(BuildContext context)? loader,
    Widget Function(BuildContext context, ResponseModel response)? errorBuilder,
    bool withRefresh = false,
    Future<void> Function()? onRefresh,
  }) =>
      RequestRefresher(
        request: this,
        builder: (context, data) => builder(context, data as T),
        onDataFitched:
            onDataFitched != null ? (data) => onDataFitched(data as T) : null,
        loader: loader,
        errorBuilder: errorBuilder,
        withRefresh: withRefresh,
        onRefresh: onRefresh,
      );
}
