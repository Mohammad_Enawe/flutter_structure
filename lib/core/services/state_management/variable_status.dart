// ignore_for_file: constant_identifier_names

enum VariableStatus {
  Loading,
  HasData,
  Error,
}

// abstract class VariableStatus {}

// class Pending extends VariableStatus {}

// class Initializing extends Pending {}

// class Fetching extends Pending {}

// class Fetched extends VariableStatus {}

// class HasData extends Fetched {}

// class NoData extends Fetched {}

// class HasError extends Fetched {}
