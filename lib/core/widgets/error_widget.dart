import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppErrorWidget extends StatelessWidget {
  final String error;
  final bool withScrolling;

  const AppErrorWidget({super.key, this.error = '', this.withScrolling = true});
  @override
  Widget build(BuildContext context) {
    if (!withScrolling) {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12.0),
              child: Text(
                error,
                maxLines: 3,
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      );
    }
    return Stack(
      children: [
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12.0),
                child: Text(
                  error,
                  maxLines: 3,
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),
        Theme(
          data: Get.theme.copyWith(
              colorScheme: Get.theme.colorScheme
                  .copyWith(secondary: Colors.transparent)),
          child: SingleChildScrollView(
              child: SizedBox(height: Get.height, width: Get.width)),
        ),
      ],
    );
  }
}
