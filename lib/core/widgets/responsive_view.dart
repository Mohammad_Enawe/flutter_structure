import 'package:flutter/material.dart';

import '../constants/breakpoints.dart';

class ResponsiveView extends StatelessWidget {
  final Widget Function(BuildContext context, BoxConstraints constraints)
      mobile;
  final Widget Function(BuildContext context, BoxConstraints constraints)
      tablet;
  final Widget Function(BuildContext context, BoxConstraints constraints)? pc;
  const ResponsiveView(
      {super.key, required this.mobile, required this.tablet, this.pc});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return LayoutBuilder(
      builder: (context, constraints) {
        if (size.width < phoneWidth) {
          return mobile(context, constraints);
        } else if (size.width < tabletWidth) {
          return tablet(context, constraints);
        } else {
          return (pc ?? tablet).call(context, constraints);
        }
      },
    );
  }
}
