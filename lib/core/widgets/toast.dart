// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:mapfiy/core/constants/localization.dart';
// import 'package:mapfiy/core/style/style.dart';

// enum ToastStatus { success, fail, warning }

// class AppToast extends StatelessWidget {
//   final String title;
//   final String message;
//   final AppLocalization locale;
//   final ToastStatus status;

//   const AppToast({
//     Key? key,
//     required this.title,
//     required this.message,
//     required this.locale,
//     this.status = ToastStatus.success,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       width: Get.width,
//       padding: locale == AppLocalization.Ar
//           ? EdgeInsets.only(right: 5)
//           : EdgeInsets.only(left: 5),
//       decoration: BoxDecoration(
//         borderRadius: BorderRadius.circular(10),
//         color: status == ToastStatus.success
//             ? Colors.green
//             : status == ToastStatus.warning
//                 ? AppStyle.orangeColor
//                 : AppStyle.redColor,
//       ),
//       child: Container(
//         padding: EdgeInsets.symmetric(vertical: 12, horizontal: 12),
//         decoration: BoxDecoration(
//           borderRadius: BorderRadius.circular(10),
//           color: AppStyle.whiteColor.withOpacity(0.3),
//         ),
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             Text(
//               title,
//               style: TextStyle(
//                 color: AppStyle.whiteColor,
//                 fontWeight: AppFontWeight.bold,
//               ),
//             ),
//             Text(
//               message,
//               style: TextStyle(
//                 color: AppStyle.whiteColor,
//                 fontWeight: AppFontWeight.medium,
//               ),
//             )
//           ],
//         ),
//       ),
//     );
//   }
// }
