import 'package:blur/blur.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Loading {
  static show() {
    Get.dialog(
      PopScope(
        canPop: false,
        child: Stack(
          children: [
            Blur(
              blur: 4,
              blurColor: Colors.white.withOpacity(0.1),
              child: const SizedBox.expand(),
            ),
            const LoadingWidget(),
          ],
        ),
      ),
      useSafeArea: true,
      barrierDismissible: false,
      barrierColor: Colors.transparent,
    );
  }

  static dispose() {
    if (Get.isDialogOpen!) {
      Get.back();
    }
  }
}

class LoadingWidget extends StatelessWidget {
  const LoadingWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }
}
