// ignore_for_file: constant_identifier_names

import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'shimmer_loading.dart';

// ignore: must_be_immutable
class AppImage extends StatelessWidget {
  final String path;
  final ImageType type;
  final BoxFit fit;
  final Widget errorWidget;
  Widget? loadingWidget;
  final BorderRadiusGeometry borderRadius;
  final Border? border;
  final double? height;
  final double? width;
  final Color? backgroundColor;
  final EdgeInsets? margin;

  AppImage({
    super.key,
    required this.path,
    required this.type,
    this.fit = BoxFit.cover,
    this.errorWidget = const Icon(Icons.error_outline),
    this.borderRadius = BorderRadius.zero,
    Widget? loadingWidget,
    this.height,
    this.width,
    this.backgroundColor,
    this.margin,
    this.border,
  }) {
    if (loadingWidget == null) {
      this.loadingWidget = ShimmerWidget.card(
        borderRadiusGeometry: BorderRadius.zero,
        height: height,
        width: width,
      );
    } else {
      this.loadingWidget = loadingWidget;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      margin: margin,
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: borderRadius,
        border: border,
      ),
      child: ClipRRect(
        borderRadius: borderRadius,
        child: Builder(
          builder: (context) {
            switch (type) {
              case ImageType.CachedNetwork:
                return CachedNetworkImage(
                  imageUrl: path,
                  fit: fit,
                  progressIndicatorBuilder: (context, url, downloadProgress) =>
                      loadingWidget!,
                  errorWidget: (context, e, i) => errorWidget,
                );
              case ImageType.Network:
                return Image.network(
                  path,
                  errorBuilder: (context, _, i) => errorWidget,
                  loadingBuilder: (BuildContext context, Widget child,
                          ImageChunkEvent? loadingProgress) =>
                      loadingWidget!,
                  fit: fit,
                );
              case ImageType.Asset:
                return Image.asset(
                  path,
                  errorBuilder: (context, _, i) => errorWidget,
                  fit: fit,
                );
              case ImageType.File:
                return Image.file(
                  File(path),
                  fit: fit,
                  errorBuilder: (context, _, i) => errorWidget,
                );
            }
          },
        ),
      ),
    );
  }
}

enum ImageType { Network, File, CachedNetwork, Asset }
