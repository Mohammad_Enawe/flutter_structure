import 'package:get/route_manager.dart';

abstract class AppRouting {
  static List<GetPage<dynamic>> routes() => [
        // GetPage(name: Pages.home.name, page: () => HomePage()),
      ];
}

enum Pages {
  login,
  home;

  String get value => '/$name';
}

abstract class Nav {
  static Future? to(Pages page, {dynamic arguments}) =>
      Get.toNamed(page.value, arguments: arguments);

  static Future? replacement(Pages page, {dynamic arguments}) =>
      Get.offNamed(page.value, arguments: arguments);

  static Future? offAll(Pages page, {dynamic arguments}) =>
      Get.offNamed(page.value, arguments: arguments);
}
