import 'package:get/get.dart';

class Messages extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'ar': {
          //messages:

          //app
          "Language": 'اللغة',
          'theme': 'الثيم',
          'no internet': 'لا يوجد إنترنت',
          'loading': 'جارِ التحميل',
          'reload': 'إعادة التحميل',
          'retry': 'إعادة المحاولة',
          'some error': 'حدث خطأ ما',
        },
        'en': {
          //messages:

          //app
          "Language": "Language",
          'theme': 'theme',
          'no internet': 'no intrnet',
          'loading': 'loading',
          'reload': 'reload',
          'retry': 'retry',
          'some error': 'some error',
        }
      };
}
