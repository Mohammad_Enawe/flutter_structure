import 'package:get/get.dart';

import '../config/app_builder.dart';
import 'localization.dart';

extension ContentTr on Map<String, dynamic> {
  String get tr {
    AppLocalization locale = Get.find<AppBuilder>().locale;
    // return this['en'];
    return this[locale.name] ?? this[AppLocalization.en.name] ?? "Not existed";
  }
}
