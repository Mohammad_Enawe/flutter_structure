import 'dart:developer';

import '../routes.dart';

abstract class Role {
  const Role();
  static Role fromString(String s) {
    switch (s) {
      case "new_user":
        return const NewUser();
      case "unregistered_user":
        return const UnregisteredUser();
      case "user":
        return const User();
      case "guest":
        return const Guest();
      default:
        throw Exception("invalid role");
    }
  }

  @override
  String toString() {
    switch (runtimeType) {
      case const (NewUser):
        return "new_user";
      case UnregisteredUser _:
        return "unregistered_user";
      case const (User):
        return "user";
      case const (Guest):
        return "guest";
      default:
        throw Exception("invalid role you must define toString function");
    }
  }

  Future initialize();

  Pages get landing;
}

//SECTION - Unregistered users
class UnregisteredUser extends Role {
  const UnregisteredUser();

  @override
  Future initialize() async {
    log("initialize unregistered user");
  }

  @override
  Pages get landing => Pages.login;
}

class NewUser extends UnregisteredUser {
  const NewUser();

  @override
  Future initialize() async {
    await super.initialize();
    log("initialize new user");
  }

  @override
  Pages get landing => Pages.login;
}
//!SECTION

//SECTION - User
class User extends Role {
  const User();
  @override
  Future initialize() async {}

  @override
  Pages get landing => Pages.home;
}

class Guest extends Role {
  const Guest();
  @override
  Future initialize() async {}

  @override
  Pages get landing => Pages.home;
}
//!SECTION