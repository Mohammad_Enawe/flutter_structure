import 'package:get/get.dart';

import 'app_builder.dart';
import 'role.dart';

abstract class RoleMiddleware {
  static bool get checkIfGuest {
    AppBuilder appBuilder = Get.find();
    if (appBuilder.role is Guest) {
      return true;
    }
    return false;
  }

  static bool get guestForbidden {
    if (checkIfGuest) {
      //open dialog
      return true;
    } else {
      return false;
    }
  }
}
