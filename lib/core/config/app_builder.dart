import 'dart:developer';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../localization/localization.dart';
import '../constants/storages_names.dart';
import '../services/rest_api/api_service.dart';
import '../style/themes.dart';
import 'defaults.dart';
import 'role.dart';

class AppBuilder extends GetxService {
  //SECTION - Data stored in storage
  String? token;
  Role role = Default.defaultRole;
  //!SECTION

  //SECTION region- Storages boxes

  GetStorage box = GetStorage(StoragesNames.app);
  final secureStorage = const FlutterSecureStorage();

  //SECTION - Load Data

  loadUserData() async {
    await box.initStorage;

    if (box.hasData("locale")) {
      locale = AppLocalization.fromString(box.read("locale"));
      if (locale != Default.defaultLocale) {
        Get.updateLocale(locale.locale);
        setLocale(locale);
      }
    }

    if (!box.hasData("theme")) {
      setTheme(Default.defaultTheme);
    } else {
      updateTheme(AppTheme.fromString(box.read("theme")));
    }

    if (box.hasData("role")) {
      role = Role.fromString(box.read("role"));
    }
    Map<String, dynamic> data = await secureStorage.readAll();
    if (data.keys.contains("token")) {
      token = data["token"];
    }
    logData();
  }

  logData() {
    log('role: $role', name: "APP BUILDER");
    log('locale: $locale', name: "APP BUILDER");
    log('locale: $theme', name: "APP BUILDER");
    log('token: $token', name: "APP BUILDER");
  }

  //!SECTION

  //SECTION - Set Data

  ///Used to set user data when login or register
  setUserData({
    required Role role,
    required String token,
  }) {
    setRole(role);
    setToken(token);
  }

  setLocale(AppLocalization locale) {
    box.write("locale", locale.value);
    this.locale = locale;
  }

  setTheme(AppTheme theme) {
    box.write("theme", theme.name);
    this.theme = theme;
  }

  ///Change user role and store in storage
  setRole(Role role) {
    box.write("role", role.toString());
    this.role = role;
  }

  setToken(String? token) {
    if (token == null) {
      secureStorage.delete(key: "token");
      this.token = null;
      return;
    }
    secureStorage.write(key: "token", value: token);
    this.token = token;
  }

  ///Used to reset user data and clean up the storage
  logout() {
    setRole(const UnregisteredUser());
    setToken(null);
  }

  //!SECTION
  //!SECTION

  //SECTION - Localization

  final Rx<AppLocalization> _locale = Default.defaultLocale.obs;
  AppLocalization get locale => _locale.value;
  set locale(AppLocalization value) => _locale.value = value;
  updateLocale() {
    if (locale.isArabic) {
      Get.updateLocale(AppLocalization.en.locale);
      setLocale(AppLocalization.en);
    } else {
      Get.updateLocale(AppLocalization.ar.locale);
      setLocale(AppLocalization.ar);
    }
  }
  //!SECTION

  //SECTION - Theme
  final Rx<AppTheme> _theme = Default.defaultTheme.obs;
  AppTheme get theme => _theme.value;
  set theme(AppTheme value) => _theme.value = value;

  updateTheme(AppTheme theme) {
    // ThemeSwitcher.of(context).changeTheme(theme: theme.theme);
    setTheme(theme);
  }
  //!SECTION

  //SECTION - Initialize app

  ///Initializer for the application
  ///you have to do navigation here
  init() async {
    await loadUserData();
    Get.put(APIService(
      token: token,
      language: locale.value,
      headers: {"Accept": "application/json"},
      withLog: true,
    ));
    await role.initialize();
    //TODO -
    // return Nav.offAll(role.landing);
  }

  //!SECTION
}
