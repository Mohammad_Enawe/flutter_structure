import 'package:flutter/services.dart';
import 'package:structure/core/localization/localization.dart';
import 'package:structure/core/config/role.dart';

import '../style/themes.dart';

abstract class Default {
  static const AppLocalization defaultLocale = AppLocalization.en;

  static const Role defaultRole = NewUser();

  static const String appTitle = '';

  static const AppTheme defaultTheme = AppTheme.light;

  static preferredOrientation() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }
}
