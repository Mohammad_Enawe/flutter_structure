import 'package:flutter/widgets.dart';

import '../constants/breakpoints.dart';

extension Responsivity on BuildContext {
  T responsive<T>(
      {required T mobile, required T tablet, T? wideTablet, T? pc}) {
    double width = MediaQuery.of(this).size.width;
    switch (width) {
      case <= phoneWidth:
        return mobile;
      case <= tabletWidth:
        return tablet;
      case <= widetabletWidth:
        return wideTablet ?? tablet;
      default:
        return pc ?? wideTablet ?? tablet;
    }
  }

  T wideResponsive<T>({required T mobile, T? tablet, T? wideTablet, T? pc}) {
    double width = MediaQuery.of(this).size.width;
    switch (width) {
      case <= phoneWidth:
        return mobile;
      case <= tabletWidth:
        return tablet ?? mobile;
      case <= widetabletWidth:
        return wideTablet ?? tablet ?? mobile;
      default:
        return pc ?? wideTablet ?? tablet ?? mobile;
    }
  }
}
