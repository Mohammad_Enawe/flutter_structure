import 'dart:async';
import 'dart:developer';
import 'dart:io';

import 'package:flutter_image_compress/flutter_image_compress.dart';

abstract class ImageUtils {
  static Future<String?> compressAndGetFile(
      File file, String targetPath) async {
    log('${file.lengthSync()}');
    XFile? result = await FlutterImageCompress.compressAndGetFile(
      file.absolute.path,
      targetPath,
      quality: 88,
      minWidth: 720,
      minHeight: 720,
    );
    log('${result?.length()}');
    return result?.path;
  }

  // static Future<String?> imagPickerWithCompression() async {
  //   ImagePicker picker = ImagePicker();
  //   XFile? picked = await picker.pickImage(source: ImageSource.gallery);
  //   if (picked != null) {
  //     return (await compressAndGetFile(File(picked.path),
  //             picked.path + '.' + picked.path.split('.').last))
  //         ?.path;
  //   }
  //   return null;
  // }

  // static Future<Uint8List> svgToPng(BuildContext context, String svgString,
  //     {int? svgWidth, int? svgHeight}) async {
  //   DrawableRoot svgDrawableRoot = await svg.fromSvgString(svgString, '');

  //   // to have a nice rendering it is important to have the exact original height and width,
  //   // the easier way to retrieve it is directly from the svg string
  //   // but be careful, this is an ugly fix for a flutter_svg problem that works
  //   // with my images
  //   String temp = svgString.substring(svgString.indexOf('height="') + 8);
  //   int originalHeight =
  //       svgHeight ?? int.parse(temp.substring(0, temp.indexOf('"')));
  //   temp = svgString.substring(svgString.indexOf('width="') + 7);
  //   int originalWidth =
  //       svgWidth ?? int.parse(temp.substring(0, temp.indexOf('"')));

  //   // toPicture() and toImage() don't seem to be pixel ratio aware, so we calculate the actual sizes here
  //   double devicePixelRatio = MediaQuery.of(context).devicePixelRatio;
  //   double width = originalWidth *
  //       devicePixelRatio; // where 32 is your SVG's original width
  //   double height = originalHeight * devicePixelRatio; // same thing

  //   // Convert to ui.Picture
  //   final picture = svgDrawableRoot.toPicture(size: Size(width, height));

  //   // Convert to ui.Image. toImage() takes width and height as parameters
  //   // you need to find the best size to suit your needs and take into account the screen DPI
  //   final image = await picture.toImage(width.toInt(), height.toInt());
  //   ByteData? bytes = await image.toByteData(format: ImageByteFormat.png);

  //   return bytes!.buffer.asUint8List();
  // }
}
