import 'package:get/get.dart';

class ActionStack<T> {
  final Function(T last) execute;
  final Duration time;
  ActionStack({
    required this.execute,
    this.time = const Duration(milliseconds: 800),
  });

  timer() async {
    int length = _queue.length;
    await time.delay();
    if (length == _queue.length) {
      execute.call(_queue.last);
      _queue = [];
    }
  }

  List<T> _queue = [];

  add(T element) {
    _queue.add(element);
    timer();
  }
}
