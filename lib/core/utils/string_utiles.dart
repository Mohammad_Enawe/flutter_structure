import 'package:flutter/material.dart';

/// Some [String] utils I have made
extension StringUtils on String {
  checkTextOverFlow({
    required int maxLines,
    required TextDirection textDirection,
    required TextAlign textAlign,
    required TextStyle style,
    required double maxWidth,
  }) {
    var tp = TextPainter(
      text: TextSpan(
        text: this,
        style: style,
      ),
      maxLines: maxLines,
      textAlign: textAlign,
      textDirection: textDirection,
    );
    tp.layout(maxWidth: maxWidth);
    return tp.didExceedMaxLines;
  }

  bool get startWithVowel {
    switch (this[0]) {
      case 'a':
      case 'e':
      case 'i':
      case 'o':
      case 'u':
      case 'A':
      case 'E':
      case 'I':
      case 'O':
      case 'U':
        return true;
      default:
        return false;
    }
  }
}
