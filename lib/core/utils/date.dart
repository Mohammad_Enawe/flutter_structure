import 'package:get/get.dart';

extension DateUtils on DateTime {
  String getdayName(int index) {
    if (index == 1) {
      return 'Monday';
    } else if (index == 2) {
      return 'Tuesday';
    } else if (index == 3) {
      return 'Wednesday';
    } else if (index == 4) {
      return 'Thursday';
    } else if (index == 5) {
      return 'Friday';
    } else if (index == 6) {
      return 'Saturday';
    } else {
      return 'Sunday';
    }
  }

  String getMonthName(int index) {
    switch (index) {
      case 1:
        return 'January'.tr;
      case 2:
        return 'February'.tr;
      case 3:
        return 'March'.tr;
      case 4:
        return 'April'.tr;
      case 5:
        return 'May'.tr;
      case 6:
        return 'June'.tr;
      case 7:
        return 'July'.tr;
      case 8:
        return 'August'.tr;
      case 9:
        return 'September'.tr;
      case 10:
        return 'October'.tr;
      case 11:
        return 'November'.tr;
      case 12:
        return 'December'.tr;
      default:
        return '';
    }
  }

  bool get isToday {
    final now = DateTime.now();
    final today = DateTime(now.year, now.month, now.day);
    final dateToCheck = DateTime(year, month, day);
    if (today == dateToCheck) return true;
    return false;
  }
}
