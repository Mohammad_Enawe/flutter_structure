import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'controller.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(SplashScreenController());
    return Material(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(horizontal: Get.width * 0.1),
        color: Colors.white,
        // child: request.builder(builder: builder),
        child: const FlutterLogo(),
      ),
    );
  }
}
