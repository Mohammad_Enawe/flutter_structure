import 'package:get/get.dart';
import 'package:structure/features/ex/index.dart';

import '../../core/config/app_builder.dart';

class SplashScreenController extends GetxController {
  AppBuilder appBuilder = Get.find();

  loadData() async {
    await appBuilder.init();

    Get.offAll(() => const ExamplesPage());
  }

  @override
  void onInit() {
    loadData();
    super.onInit();
  }
}
