import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:structure/core/services/rest_api/models/response_model.dart';

class PaginationTestPageController extends GetxController {
  Future<ResponseModel> loadFakeData(int page, CancelToken cancel) async {
    await 2.seconds.delay();
    if (page < 4) {
      return ResponseModel(
          success: true,
          message: "",
          data: List.generate(
              3, (index) => {"data": "page:$page, content: $index"}));
    } else {
      return ResponseModel(success: true, message: "", data: []);
    }
  }
}
