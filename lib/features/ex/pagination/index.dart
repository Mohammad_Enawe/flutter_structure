import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:structure/core/services/pagination/options/list_view.dart';

import 'controller.dart';

class PaginationTestPage extends StatelessWidget {
  PaginationTestPage({super.key});

  final ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    PaginationTestPageController controller =
        Get.put(PaginationTestPageController());
    return Scaffold(
      appBar: AppBar(),
      body: CustomScrollView(
        controller: scrollController,
        slivers: [
          SliverList(
            delegate: SliverChildListDelegate(
              [
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 24),
                  child: Text(
                    "Sliver Test",
                    style: TextStyle(fontSize: 36),
                  ),
                ),
              ],
            ),
          ),
          ListViewPagination<String>.sliver(
            tag: "tag",
            fetchApi: controller.loadFakeData,
            fromJson: (s) => s['data'],
            scrollController: scrollController,
            itemBuilder: (context, index, s) {
              return Container(
                height: 100,
                margin: const EdgeInsets.only(bottom: 8),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.deepPurpleAccent,
                ),
                child: Center(
                    child:
                        Text(s, style: const TextStyle(color: Colors.white))),
              );
            },
          )
        ],
      ),
    );
    // return Scaffold(
    //   appBar: AppBar(),
    //   body: ListViewPagination<String>.separated(
    //     tag: "tag",
    //     fetchApi: controller.loadFakeData,
    //     fromJson: (s) => s['data'],
    //     separatorBuilder: (_, __) => const SizedBox(height: 12),
    //     itemBuilder: (context, index, s) {
    //       return Container(
    //         height: 100,
    //         decoration: BoxDecoration(
    //           borderRadius: BorderRadius.circular(10),
    //           color: Colors.deepPurpleAccent,
    //         ),
    //         child: Center(
    //             child: Text(s, style: const TextStyle(color: Colors.white))),
    //       );
    //     },
    //   ),
    // );
  }
}
