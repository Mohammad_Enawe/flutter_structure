import 'package:animated_theme_switcher/animated_theme_switcher.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:structure/core/config/app_builder.dart';
import 'package:structure/core/style/themes.dart';

import 'controller.dart';

class MainSettingsPage extends StatelessWidget {
  const MainSettingsPage({super.key});

  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    MainSettingsPageController controller =
        Get.put(MainSettingsPageController());
    AppBuilder appBuilder = Get.find();
    return ThemeSwitchingArea(
      child: Scaffold(
        appBar: AppBar(),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SwitchListTile(
              value: appBuilder.locale.isArabic,
              onChanged: (_) => appBuilder.updateLocale(),
              title: Text("Language".tr),
            ),
            ThemeSwitcher(
              builder: (context) => SwitchListTile(
                value: appBuilder.theme.isLight,
                onChanged: (_) {
                  appBuilder.updateTheme(appBuilder.theme.isLight
                      ? AppTheme.dark
                      : AppTheme.light);
                  ThemeSwitcher.of(context)
                      .changeTheme(theme: appBuilder.theme.theme);
                },
                title: Text("${"Theme".tr} ${appBuilder.theme.name}"),
              ),
            )
          ],
        ),
      ),
    );
  }
}
