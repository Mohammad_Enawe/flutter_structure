import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:structure/features/ex/main_settings/index.dart';

import 'pagination/index.dart';

class ExamplesPage extends StatelessWidget {
  const ExamplesPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Examples")),
      body: ListView(
        children: [
          InkWell(
            onTap: () => Get.to(() => const MainSettingsPage()),
            child: const Card(
              margin: EdgeInsets.all(8),
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Text("Main settings"),
              ),
            ),
          ),
          InkWell(
            onTap: () => Get.to(() => PaginationTestPage()),
            child: const Card(
              margin: EdgeInsets.all(8),
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Text("Pager example"),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
